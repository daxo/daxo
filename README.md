# daxo.dev's personal website source code

Code for a personal site: https://daxo.dev

## License

While the source code in this repository is licensed per the `LICENSE` file,
please note that any of my own artwork I post, I retain as copyright Jonathan
Zacsh. Contact me to talk about using it - I'm happy to chat!

## Development

**tl;dr** _all_ human-written git commits should happen on branch `main` (once
you're happy with the local build via `make` local-testing) and `make deploy`
should handle the rest of the commits/deploys/push details.

This is a a repository of mostly text _(written in asciidoc)_ - and a tiny bit
of source code - that is automatically turned into a directory of static HTML
assets using:
  1. `hugo` binary to compile and manage content. last used
    - `hugo v0.135.0+extended linux/amd64 BuildDate=unknown VendorInfo=nixpkgs`
    - `go version go1.23.2 linux/amd64`
  1. everything out-of-hugo via Make; eg:
    - simple Make commands: `build` and `deploy`
    - `src/` containing _all_ of the things make recipes depend on
       - _except_ typescript that unfortunately gets compiled
         directly in static directories, so therefore gets served out of them.
         This was a good-enough solution [at the time I figured it
         out](https://gitlab.com/jzacsh/glue#hugo-ts), but I'm happy to have
         this fixed so the TS lives somewhere else.

### Git Branches

  1. Source & content: `main` branch
  2. generated website: `www` branch

### Commands:

Build static content of this repo into HTTP static files for serving:
```bash
make build
```

#### Advanced

To make changes and live reload in your browser:
```bash
hugo server
```

To see final (CDN-ready) contents of `make build`:
```bash
xdg-open tmp/  # inspect

python3 -m http.server  # and serve it, if you'd like
```

#### Setup

Just one-time, after *first* cloning this codebase:
```bash
go get github.com/spf13/hugo
```

### Deploying

**tl;dr** hosting is _currently_ done by gitlab sites CDN. Commands below in
this section and then _other_ CDNs instructions further down for posterity.

History of deploys can be seen on the `www` branch
[history in gitlab's "graph" pane](https://gitlab.com/daxo/daxo.gitlab.io/-/network/www)


To automatically generate _(`make buildall`)_ and deploy _(`git {commit,push}`)_
a new `www` branch to gitlab:
```bash
make deploy
# if adapting, then read the variables passed inside this make target's script,
# first!
```
