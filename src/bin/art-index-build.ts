#!/usr/bin/env -S deno run --allow-write --allow-env --allow-read --allow-run

import {
  ArtworkMetadata,
  isValidArtworkMetadataKey,
  ArtworkDir,
  ArtworkMetadataFreq,
  ArtworkMetadataStats,
  Artwork,
  ArtworkIndex,
  buildDefaultValueFreq,
} from './ts/artapi.ts';

import {
  StaticHost,
  TargetSmallWidthPx,
} from './ts/art.ts';

import {assert} from './ts/assert.ts';

import {
  keepNDecimals,
  digestOf,
} from './ts/common.ts';

import {
  fsPathJoin,
} from './ts/fs.ts';

import {
  gitFindOrigin,
  OPTS as GitOpts,
} from './git.ts';

import {
  Geometry2D,
  NotAnImageError,
  getImageSize,
} from './image.ts';

import {
  resizeImageWidth,
} from './resize.ts';

import {readTextFileIfExists} from './file.ts'

/**
 * Generates a mildly useful JSON tree of the image that should be available as
 * as assets to reference (eg: from <img /> tags) without any issue.
 *
 * NOTE: this was documented as working as of:
 *     ```sh
 *     $ deno --version
 *     deno 1.29.1 (release, x86_64-unknown-linux-gnu)
 *     v8 10.9.194.5
 *     typescript 4.9.4
 *     ```
 */

interface ProgOpts {
  staticHost: StaticHost;

  // Whether we'll only lightly sample directories, and print lots of annoying
  // logs.
  isDebugging: boolean;
}

const OPTS = {
  DbgModeMaxChildren: 20,
  MinMetadataValueThreshold: 5,
  MetadataFileSuffix: 'metadata',
  DecimalPrecision: 3,
  ExpectedImageSuffixes: [
    'jpg', 'jpeg', 'png', 'webp',
    'gif', 'svg', 'tif',
  ],
};

function buildProgOptions(args: Array<string>): ProgOpts {
  const usageHelpStr = 'usage: [-debug[:DisableParseGitDiffNameOnly]] HUGO_DIR';
  assert(Deno.args.length, `expected a directory of art: ${usageHelpStr}`);
  let isDebugging = false;
  let filepathArgIndex = 0;
  if (Deno.args.length == 2) {
    assert(Deno.args[0].startsWith('-debug'), usageHelpStr);
    filepathArgIndex = 1;
    isDebugging = true;
    GitOpts.ParseGitDiffNameOnly = !Deno.args[0].includes('DisableParseGitDiffNameOnly');
  }
  let filepathArg = (Deno.args[filepathArgIndex] || '').trim();

  GitOpts.isDebugging = isDebugging;
  return {
    staticHost: new StaticHost(filepathArg),
    isDebugging,
  };
}

const FLAGS = buildProgOptions();

///////////////////////////////////////////////////////////
// basic, business-agnostc lib ////////////////////////////

const stdout = console.log;
const stderr = console.warn;
function _logPrefix(logger: Function, prefix: string, msg: string, ...opts: unknown) {
  const format = `${prefix}: ${msg}`;
  logger(format, ...opts);
}

// specifically use stderr because stdout should only be used for JSON data itself
const info = (...args) => _logPrefix(stderr, 'STATUS', ...args);
const warn = (...args) => _logPrefix(stderr, 'WARNING', ...args);
const dbg = (...args) => {
  if (!FLAGS.isDebugging) return;
  _logPrefix(stderr, 'DBG', ...args);
}

// only useful for debugging
async function softTry<T>(fn, fallback: T): T {
  try {
    return await fn();
  } catch (e: unknown) {
    console.error(`MASKING FAILURE!! got\n"""\n${e}\n"""\n`);
    return fallback;
  }
}

// end of basic, business-agnostic lib ////////////////////
///////////////////////////////////////////////////////////

function prepFloat(num: number): number {
  return keepNDecimals(num, OPTS.DecimalPrecision);
}

/**
 * Computes a hash for `artworkFilePath` that is unlikely to break by accident.
 */
async function computeStableId(artworkFilePath: string): string {
  const gitOrigin: GitOrigin = await gitFindOrigin(artworkFilePath);
  // Include *both* original commit and filepath because original commit itself
  // could have other files in it as well, so that alone could cause such an ID
  // to collide with other files that would use this same computation.
  const uniqueIdOfFile: string = [
    gitOrigin.originatingCommitId,
    gitOrigin.originalFilepath,
  ].join('|');

  const digestOfUniqueId: string = await digestOf(uniqueIdOfFile);
  return digestOfUniqueId;
}

function buildMetadataPath(filepath: string): string {
  return `${filepath}.${OPTS.MetadataFileSuffix}`;
}

function startNewArtdir(filename: string): ArtworkDir {
  return {
    filename,
    children: [],
    immediateLength: 0,
    recursiveLength: 0,
  }
}

function addChildToArtdir(artDir: ArtworkDir, subDir: ArtworkDir) {
  artDir.children.push(subDir);

  // TODO this calc. is flawed, so if I ever care about this, rewrite this;
  // (test case: put a file in a deeply nested directory, the parents of which
  // have no contents of their own).
  artDir.recursiveLength = 0;
}

function calculateRatio(width: number, height: number): number {
  if (height <= 0 || !width) return 0; // avoid division by zero
  const rawRatio = width / height;

  return prepFloat(rawRatio);
}

function buildArtwork(
  filename: string,
  artworkId: string,
  width: number,
  height: number,
  metadata: ArtworkMetadata,
): Artwork {
  return {
    filename,
    artworkId,
    ratio: calculateRatio(width, height),
    width,
    height,
    metadata,
  };
}

interface Dir {
  rootPath: string;
  entry: Deno.DirEntry;
}

class Queue {
  rootDir: string;
  private q: Array<Dir>;

  constructor(rootPath: string) {
    assert(
      (rootPath || '').trim().length,
      'rootPath cannot be empty');
    this.rootDir = rootPath;
    this.q = [];
  }

  addDir(dir: Dir) {
    this.q.push(dir);
  }

  next(): Dir {
    assert(!this.isEmpty(), 'called next() on empty queue');
    return this.q.shift();
  }

  isEmpty() {
    return !this.size();
  }

  size() {
    return this.q.length;
  }
}

async function loadMetadataForArtwork(artworkFilePath: string): ArtworkMetadata|null {
  const metadataFilePath = buildMetadataPath(artworkFilePath);
  const contents: string = await readTextFileIfExists(metadataFilePath);
  if (contents === null) return null;
  assert(String(contents).trim(), `found effectively-empty metadata file: ${metadataFilePath}`);

  const lines = contents.
      split('\n').

      // Encode original file's line numbers, for future error messages
      map((line, index) => {
        return {line, index};
      });

  function parserMsg(lineNumber: number, errorMessage: string) {
    return `metadata parsing error on line ${
        lineNumber} of "${
        metadataFilePath}": ${
        errorMessage}; line:\n"${
        lines[lineNumber].line}"`;
  }

  return lines.

      // Remove empty and comment lines
      filter(ln => ln.line && ln.line.trimStart()[0] !== '#' && ln.line.trim()).

      // Parse keys and values
      map(ln => {
        const keyValDelim = ':';
        let parts = ln.line.split(keyValDelim);
        assert(
            parts.length > 1,
            () => parserMsg(ln.index, `missing a key-value delimeter('${keyValDelim}')`));
        if (parts.length !== 2) {
          warn('consuming blindly: : '+ parserMsg(ln.index, `too many key-value delimeters('${keyValDelim}')`));
          parts = [
            parts[0],
            ln.line.substr(parts[0].length + 1 /*include the delim itself*/),
          ];
        }
        const [key, val] = parts.map(part => part.trim());
        assert(key, () => parserMsg(ln.index, `empty key (line="${ln.line}")`));
        assert(val, () => parserMsg(ln.index, `empty value (line="${ln.line}")`));
        assert(
          isValidArtworkMetadataKey(key),
          () => parserMsg(`unregistered key ("${key}"); typo? if not, just add it to ArtworkMetadata definition`));

        return {
          ...ln,
          key,
          val
        }
      }).

      // Collect keys and values into ArtworkMetadata object
      reduce((metadata, ln) => {
        metadata[ln.key] = metadata[ln.key] || [];
        metadata[ln.key].push(ln.val);
        return metadata;
      }, {} /*initialValue*/);
}

async function processDirEntry(pathToList: string, q: Queue, entry?: Deno.DirEntry): ArtworkDir {
  assert(!entry || entry.isDirectory, 'processDirEntry called on a non-dir: ${dir.name}');

  dbg(`pathToList="${pathToList}"`);
  const artDir: ArtworkDir = startNewArtdir(pathToList);
  let directChildCount = 0;
  for await (const dirEntry of Deno.readDir(pathToList)) {
    if (FLAGS.isDebugging && directChildCount > OPTS.DbgModeMaxChildren) break;
    directChildCount++;

    if (dirEntry.isDirectory) {
      dbg(`under "${pathToList}" got DIR: "${dirEntry.name}" (q size ${q.size()})`);
      q.addDir({rootPath: fsPathJoin(pathToList, dirEntry.name), entry: dirEntry});
      continue;
    }

    let artworkFilePath = fsPathJoin(pathToList, dirEntry.name);
    if (artworkFilePath.endsWith(OPTS.MetadataFileSuffix)) continue;

    let geometry: Geometry2D|NotAnImageError = await getImageSize(
        artworkFilePath,
        OPTS.ExpectedImageSuffixes);
    if (geometry instanceof NotAnImageError) {
      warn(`skipping over non-art file found: "${artworkFilePath}"`);
      continue;
    }

    dbg(`under "${pathToList}" got FILE: "${dirEntry.name}" (q size ${q.size()})`);
    // TODO consider Promise.all() for both these filesystem isnpections, so we
    // can get _slightly_ faster build times
    const artworkId: string = await computeStableId(artworkFilePath);

    artDir.immediateLength++;
    const artworkMetadata: ArtworkMetadata|null = await loadMetadataForArtwork(artworkFilePath);
    cacheStats(artworkMetadata);

    const artwork = buildArtwork(
      artworkFilePath,
      artworkId,
      geometry.width,
      geometry.height,
      artworkMetadata);

    await resizeImageWidth(
        {filepath: artworkFilePath, geo: geometry},
        FLAGS.staticHost.toThumbPath(artworkFilePath),
        TargetSmallWidthPx * 2);

    addChildToArtdir(artDir, artwork);
  }
  // initial baseline; this number will be ammended by further walks
  artDir.recursiveLength = artDir.immediateLength;

  while (!q.isEmpty()) {
    const nextDirToWalk = q.next();
    const subPath = nextDirToWalk.rootPath;
    const subDirEntry = nextDirToWalk.rootPath;
    dbg(`walking, "${subPath}", ${q.size()}`);
    const subArtworkDir = await processDirEntry(subPath, new Queue(subPath))
    if (subArtworkDir.children.length === 0) {
      continue;
    }
    addChildToArtdir(artDir, subArtworkDir);
    artDir.recursiveLength += subArtworkDir.recursiveLength;
  }
  artDir.children.sort((a, b) => {
    if (a.filename === b.filename) return 0;
    return a.filename < b.filename ? -1 : 1
  });
  return artDir;
}

async function computeMetadataStats(artworksMetadata: Array<ArtworkMetadata>, artworkCount: number): ArtworkMetadataStats {
  assert(
      artworksMetadata.length <= artworkCount,
      `expected no more metadata (got ${artworksMetadata.length} than known works of art (${artworkCount})`);

  // TODO add special-casing for the "created" metadata so meaningful filtering
  // can happen (eg: by year, by month

  const rawStats: Map<string, Map<string, ArtworkMetadataFreq>> = await artworksMetadata.
      reduce(async (rawMapOfValuesAsync, metadata) => {
        const rawMapOfValues = await rawMapOfValuesAsync;
        for (let key of Object.keys(metadata)) {  // "key" here is eg: "created", "color", "type"
          const extantStats: Map<string, ArtworkMetadataFreq> = await metadata[key].reduce(async (extantStatsAsync, value) => {
            const extantStats = await extantStatsAsync;
            let stat: ArtworkMetadataFreq = extantStats.get(value) || await buildDefaultValueFreq(value);
            stat.count++;
            extantStats.set(value, stat);
            return extantStats;
          }, rawMapOfValues.get(key) || new Map());
          rawMapOfValues.set(key, extantStats);
        }
        return rawMapOfValues;
      }, new Map() /*initialValue*/);

  return Array.
      from(rawStats.entries()).
      reduce((retainedStats, [metadataKey, rawMapOfValues]) => {
        let frequentedValues: Array<ArtworkMetadataFreq> = Array.
            from(rawMapOfValues.values()).
            filter(stat => stat.count >= OPTS.MinMetadataValueThreshold).
            map(stat => {
              stat.freq = prepFloat(stat.count / artworkCount);
              return stat;
            });

        retainedStats[metadataKey] = frequentedValues;
        return retainedStats;
      }, {} /*initialValue*/);
}

// end of business logic libs /////////////////////////////
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Start of main executions ///////////////////////////////

const cachedStats: Array<ArtworkMetadata> = [];
function cacheStats(metadata: ArtworkMetadata) {
  cachedStats.push(metadata);
};

const q = new Queue(FLAGS.staticHost.sketchbookDir);
info(`starting crawl of art dir: ${FLAGS.staticHost.sketchbookDir}`);
// TODO delete all old thumbnails before index building starts
const index: ArtworkIndex = {
  contents: await processDirEntry(q.rootDir, q),
};
info(`done with crawl of art dir; computing stats`);
index.metadataStats = await computeMetadataStats(cachedStats, index.contents.recursiveLength);

info(`done building index; printing`);

await Deno.writeTextFile(
  FLAGS.staticHost.indexPath,
  JSON.stringify(index));
