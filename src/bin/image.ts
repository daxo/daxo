import {
  CommandlineError,
  loadLinesForCommand,
} from './shell.ts';

import {assert} from './ts/assert.ts';

import {
  fsFileExt,
} from './ts/fs.ts';

export interface Geometry2D {
  width: number;
  height: number;
}

export class NotAnImageError extends Error {
  filepath: string;
  constructor(filepath: string, message: string) {
    super(message);
    this.filepath = filepath;
  }

  static fromImageMagicErr(filepath: string): NotAnImageError {
    return new NotAnImageError(`imagemagick error: probably not an image: ${filepath}`);
  }
}

function isExpectedImageSuffix(filepath: string, expectedImageExts: Array<string>): boolean {
  const actualFileExt: string = fsFileExt(filepath);
  return expectedImageExts.includes(actualFileExt);
}

// Note: an alternative for width and height information for the SVG files is to
// use the width= and height= attribute that inkscape embeds into the svg root
// node. this can be read with an xpath expression passed to xmllint, like so:
//
//   ```bash
//   $ heighNum="$(xmllint --xpath 'string(//@height)' my.svg)"
//   $ widthNum="$(xmllint --xpath 'string(//@width)' my.svg)"
//   ```
export async function getImageSize(
  filepath: string,
  expectedImageExts: Array<string>,
): Geometry2D|NotAnImageError {
  /**
   * See identify(1) utility from imagemagick. eg:
   *   ```sh
   *   $ identify -format '%[h]'
   *   $ identify -format '%[w]'
   *   ```
   */
  let cliResult: Array<string>;
  try {
    cliResult = await loadLinesForCommand([
      'identify',
      '-format',
      '%[w]\n%[h]',
      filepath,
    ]);
  } catch (cliError) {
    if (cliError instanceof CommandlineError &&
        !isExpectedImageSuffix(filepath, expectedImageExts)) {
      return NotAnImageError.fromImageMagicErr(filepath);
    }
    throw e;
  }

  assert(
      cliResult.length === 2,
      `expected width and height line for ${filepath} but got ${cliResult.length} lines from imagemagick:\n"""\n${cliResult.join('\n')}\n"""\n`);

  function parsePixels(pixelStr: string, context: string): number {
    const result = parseInt(pixelStr, 10);
    assert(!isNaN(result), `failed parsing imagemagick's ${context} on ${filepath}`);
    return result;
  }
  return {
    width: parsePixels(cliResult[0], 'width'),
    height: parsePixels(cliResult[1], 'height'),
  };
}
