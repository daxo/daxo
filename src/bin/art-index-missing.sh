#!/usr/bin/env bash

# shellcheck disable=SC1091
source yabashlib || {
  printf 'missing yabashlib dependency\n' >&2
  exit 1
}

opt_excludePattern="$1"
function excludeLines() (
  { (( $# > 0 )) && strIsContentful "$1" ; } || {
    cat
    return
  }
  local delete_expr='/'"$1"'/d'
  set -x
  sed --regexp-extended --expression "$delete_expr"
)

mapfile -t sketchbook < <(
  find static/img/artwork/sketchbook/ -type f -print |
    excludeLines '^(copying)$' |
    excludeLines '\.metadata$' |
    excludeLines "$opt_excludePattern"
)

for f in "${sketchbook[@]}"; do
  m="$f".metadata; printf 'processing %s\n' "$f"
  if [[ -e "$m" && "$(wc -l < "$m")" -gt 0 ]]; then
    printf 'skipping metadata already exists: %s\n' "$f"
    continue
  fi
  feh --scale --image-bg '#ffffff' "$f" &
  "${EDITOR:-vim}" "$f".metadata
  file "$f".metadata
done
