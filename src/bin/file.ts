/** Returns text content of a `filePath` if it exists, otherwise returns null. */
export async function readTextFileIfExists(filePath: string): string|null {
  try {
    return await Deno.readTextFile(filePath);
  } catch (e) {
    if (e instanceof Deno.errors.NotFound) {
      return null;
    }
    throw e;
  }
}
