TMPSRV    := tmp
BINDIR    := src/bin

TAGHASH   := TAG_VERSION_HASH
TAGTREE   := TAG_VERSION_TREE
TAGDATE   := TAG_VERSION_DATE
GIT_HEAD  := `git symbolic-ref --short HEAD`
GIT_VER   := $(shell git show-ref --hash heads/$(GIT_HEAD) | tee | cut -c 1-10)
GIT_TREE  := "https://gitlab.com/daxo/daxo.gitlab.io/-/tree/$(GIT_VER)"
GIT_DATE  := `git show -s --format='%cd' $(GIT_VER)`

STATIC_DIR_ROOT := static

# TODO nitpick: reorganize recipes so hugo (`compile`) doesn't run twice in this
# step.
devserver: buildsome
	hugo server

buildsome: compile sidecar_tag

# superset of build steps; this is split in half because some of the steps are
# painfully slow and only necessary when deploying but not when locally
# developing.
buildall: buildsome

compile: setupbuild
	hugo

setupbuild: clean
	mkdir -p $(TMPSRV)/

sidecar_tag: TAG_SRC=$(shell find $(TMPSRV) -type f -name '*.html')
sidecar_tag:
	sed --in-place "s|$(TAGHASH)|$(GIT_VER)|"  $(TAG_SRC)
	sed --in-place "s|$(TAGTREE)|$(GIT_TREE)|" $(TAG_SRC)
	sed --in-place "s|$(TAGDATE)|$(GIT_DATE)|" $(TAG_SRC)

clean:
	$(RM) -rf $(TMPSRV)/*

deploy: buildall
	$(BINDIR)/gitlab-pages.sh $(GIT_VER) $(TMPSRV)

.PHONY: devserver deploy clean sidecar_tag buildall buildsome setupbuild compile
