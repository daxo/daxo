export function assert<T>(content: T|null|undefined|false, errorMsgOrFn: string|Function): T {
  if (!!content) return content;
  const errorMsg = typeof errorMsgOrFn === 'function' ? errorMsgOrFn() : errorMsgOrFn;
  throw new Error(`assertion failed: ${errorMsg}`);
}
