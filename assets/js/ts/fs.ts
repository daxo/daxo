import {assert} from './assert.ts';
import {strTrim, strLastChar} from './string.ts';

export const FsDelim = '/';

function fsDelimTrim(path: string): string {
  return strTrim(path, FsDelim);
}

export function fsPathJoin(...pathPart: string): string {
  const parts = Array.of(...pathPart);
  assert(parts.length >= 2, `only ${parts.length} path parts; nothing to join`);

  const cleanedParts: string = parts.
      map(part => fsDelimTrim(part)).
      filter(part => part);
  assert(parts.length >= 2, `only ${parts.length} path parts; nothing to join`);

  const leadingDelim: boolean = parts[0][0] === FsDelim;
  if (leadingDelim) cleanedParts.splice(0 /*start*/, 0 /*deleteCount*/, '');

  const trailingDelim: boolean = strLastChar(parts[parts.length - 1]) === FsDelim;
  if (trailingDelim) cleanedParts.push('');

  return cleanedParts.join(FsDelim);
}

export function fsFileExt(filepath: string): string {
  const fileSuffixMatch =
      assert(
          baseName(filepath),
          `could not determine basename of "${filepath}"`).
      match(/\.(\w+)$/);
  if (!fileSuffixMatch) return '';
  return fileSuffixMatch[1].toLowerCase().trim();
}

export function replaceExtension(
  path: string,
  toExt: string,
): string {
  const fromExt: string = assert(
      fsFileExt(path),
      `can only extension on file with one present (got file "${
        path}")`);
  const replaceRegexp = new RegExp(`\.${fromExt}$`);
  return path.replace(replaceRegexp, `.${toExt}`);
}

export function dirName(path: string): string {
  return pathParts(path).dirname || '';
}

export function baseName(path: string): string {
  return pathParts(path).basename;
}

export interface PathParts {
  dirname: string;
  basename: string;
}

export function pathParts(pathRaw: string): PathParts {
  const path: string = String(pathRaw).trim();
  assert(path.length, `path must be non-empty; got: "${pathRaw}"`);

  const parts: Array<string> = path.split(FsDelim);
  if (parts.length === 1) {
    return {basename: path};
  }

  const lastIndex = parts.length - 1;

  return {
    dirname: parts.slice(0, lastIndex).join(FsDelim),
    basename: parts[lastIndex],
  }
}
