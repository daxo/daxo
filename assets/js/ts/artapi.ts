import {digestOf} from './common.ts';

type ArtworkMetadataValues = Array<String>;

/**
 * To find currently utilized values - of which if there are no typos, they
 * should be in this interface - run this command:
 *    $ cut -f 1 -d ' ' static/img/artwork/sketchbook/*.metadata  | sort | uniq
 */
export interface ArtworkMetadata {
  color: ArtworkMetadataValues;
  content: ArtworkMetadataValues;
  created: ArtworkMetadataValues;
  media: ArtworkMetadataValues;
  type: ArtworkMetadataValues;

  // WARNING: adding something here should correspond to adding something in
  // isValidArtworkMetadataKey and ArtworkMetadataStats
}

export function isValidArtworkMetadataKey(key: string): boolean {
  return [
    'color',
    'content',
    'created',
    'media',
    'type',
  ].includes(key);
}

interface ArtworkIndex {
  contents: ArtworkDir,
  metadataStats: ArtworkMetadataStats,
}

interface FsDirEntry {
  filename: string;
}

export interface ArtworkDir extends FsDirEntry {
  // TODO this should be Array<Artwork|ArtworkDir>
  children: Array<FsDirEntry|ArtworkDir>;

  // Count of the direct child files (not dirs).
  immediateLength: number;

  // Count of all files (not dirs) recursively.
  recursiveLength: number;
}

export interface ArtworkMetadataFreq {
  value: string;
  hash: string; // eg: md5sum of value
  freq: number; // float in (0,1]
  count: number; // natural number
}

export interface ArtworkMetadataStats {
  color: Array<ArtworkMetadataFreq>;
  content: Array<ArtworkMetadataFreq>;
  created: Array<ArtworkMetadataFreq>;
  media: Array<ArtworkMetadataFreq>;
  type: Array<ArtworkMetadataFreq>;
}

export interface Artwork extends FsDirEntry {
  artworkId: string;
  ratio: number;
  width: number;
  height: number;
  metadata?: ArtworkMetadata;
}

export async function buildDefaultValueFreq(value: string): ArtworkMetadataFreq {
  const hash: string = await digestOf(value);
  return {
    value,
    hash,
    count: 0,
    freq: 0,
  };
}
